package ru.vtb.teachings.springteachings.shop.configuration;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
@Data
@Validated
@NoArgsConstructor
@ConfigurationProperties(prefix = "shop")
public class ShopProperties {

    @NotEmpty(message = "Поле имя должно быть заполнено")
    private String name;

    @Size(max = 10)
    private String workingHours;

}
