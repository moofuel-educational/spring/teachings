package ru.vtb.teachings.springteachings.shop.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vtb.teachings.springteachings.shop.domain.entity.Product;
import ru.vtb.teachings.springteachings.shop.domain.dto.ProductDto;
import ru.vtb.teachings.springteachings.shop.service.ShopService;

import java.util.List;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
@RestController
@RequestMapping("/shop")
public class ShopController {

    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping(value = "/get-all")
    public List<Product> getProducts() {
        return this.shopService.getProducts();
    }

    @PostMapping("/store")
    public void storeNewProducts(@RequestBody List<Product> products) {
        this.shopService.storeProducts(products);
    }

    @PutMapping("/products/{id}")
    public Product updateProduct(@PathVariable Long id,  @RequestBody ProductDto productDto) {
        return this.shopService.updateProduct(id, productDto);
    }
}
