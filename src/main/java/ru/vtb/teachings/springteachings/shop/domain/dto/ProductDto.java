package ru.vtb.teachings.springteachings.shop.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private String name;
}
