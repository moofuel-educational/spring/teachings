package ru.vtb.teachings.springteachings.shop.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ProductNotFoundException extends IllegalStateException {

    public ProductNotFoundException(String message) {
        super(message);
    }
}
