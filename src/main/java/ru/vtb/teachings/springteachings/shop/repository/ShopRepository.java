package ru.vtb.teachings.springteachings.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vtb.teachings.springteachings.shop.domain.entity.Product;


/**
 * @author dvivanov
 * @since 17.09.2021
 */
@Repository
public interface ShopRepository extends JpaRepository<Product, Long> {
}
