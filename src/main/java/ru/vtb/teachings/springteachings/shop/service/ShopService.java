package ru.vtb.teachings.springteachings.shop.service;

import ru.vtb.teachings.springteachings.shop.domain.entity.Product;
import ru.vtb.teachings.springteachings.shop.domain.dto.ProductDto;

import java.util.List;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
public interface ShopService {

    List<Product> getProducts();

    void storeProducts(List<Product> products);

    Product updateProduct(Long id, ProductDto product);
}
