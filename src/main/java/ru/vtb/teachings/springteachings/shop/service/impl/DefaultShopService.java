package ru.vtb.teachings.springteachings.shop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.vtb.teachings.springteachings.shop.configuration.ShopProperties;
import ru.vtb.teachings.springteachings.shop.domain.entity.Product;
import ru.vtb.teachings.springteachings.shop.domain.dto.ProductDto;
import ru.vtb.teachings.springteachings.shop.domain.exception.ProductNotFoundException;
import ru.vtb.teachings.springteachings.shop.repository.ShopRepository;
import ru.vtb.teachings.springteachings.shop.service.ShopService;

import java.util.List;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
@Service
@RequiredArgsConstructor
public class DefaultShopService implements ShopService {

    private final ShopRepository shopRepository;
    private final ShopProperties properties;

    @Override
    public List<Product> getProducts() {
        return shopRepository.findAll();
    }

    @Override
    public void storeProducts(List<Product> products) {
        shopRepository.saveAll(products);
    }

    @Override
    public Product updateProduct(Long id, ProductDto productDto) {
        final Product productForUpdate = shopRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException("Product was not found"));
        productForUpdate.setName(productDto.getName());
        return shopRepository.save(productForUpdate);
    }
}
