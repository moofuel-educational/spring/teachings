package ru.vtb.teachings.springteachings.shop;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.vtb.teachings.springteachings.shop.domain.entity.Product;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dvivanov
 * @since 17.09.2021
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ShopControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void getProductsShouldReturnEmptyListWhenDBIsEmpty() {

        final ResponseEntity<List<Product>> exchange = restTemplate.exchange("/shop/get-all", HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                });
        final HttpStatus statusCode = exchange.getStatusCode();
        assertThat(statusCode)
                .isEqualTo(HttpStatus.OK);
        final List<Product> actual = exchange.getBody();
        final List<Product> expected = Collections.emptyList();

        assertThat(actual)
                .isEqualTo(expected);
        
    }
}
